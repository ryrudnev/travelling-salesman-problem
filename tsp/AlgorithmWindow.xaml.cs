﻿using System.Windows;

namespace tsp
{
    /// <summary>
    /// Логика взаимодействия для AlgorithmWindow.xaml
    /// </summary>
    public partial class AlgorithmWindow : Window
    {
        public AlgorithmWindow()
        {
            InitializeComponent();
        }

        private void ClickOkBttn(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }
    }
}
