﻿using System.Windows;

namespace tsp
{
    /// <summary>
    /// Логика взаимодействия для AboutProgramm.xaml
    /// </summary>
    public partial class AboutWindow : Window
    {
        public AboutWindow()
        {
            InitializeComponent();
        }

        private void okBttnClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }
    }
}
